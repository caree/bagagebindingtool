﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace BagageBindingTool
{
    public partial class frmSetting : Form
    {
        public frmSetting()
        {
            InitializeComponent();

            this.txtHost.Text = Program.Host;
            this.txtToken.Text = Program.Token;

            this.btnExit.Click += btnExit_Click;
        }

        void btnExit_Click(object sender, EventArgs e)
        {
            var token = this.txtToken.Text;
            if (token == string.Empty)
            {
                MessageBox.Show("请填写一个有效的用户识别编码！");
                return;
            }

            var host = this.txtHost.Text;
            var pattern = @"http://([0-9]+\.){3}[0-9]+\:[0-9]+/";
            if (host == string.Empty || !Regex.IsMatch(host, pattern))
            {
                MessageBox.Show("请填写一个有效的服务地址！");
                return;
            }
            nsConfigDB.ConfigDB.saveConfig("host", host);
            nsConfigDB.ConfigDB.saveConfig("token", token);
            Program.Token = token;
            Program.Host = host;

            this.Close();
        }
    }
}
