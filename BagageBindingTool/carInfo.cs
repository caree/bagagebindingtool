﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BagageBindingTool
{
    public class carInfo
    {
        public string carID;
        public string carType;
        public string note;
        public string timeStamp;
        public string bagageBinded;

        public carInfo() { }
    }
    //[{"bagageID":"demo002","timeStamp":"2014-04-29 15:40:14","note":"","carID":"xcar2"}]
    public class bagageInfo
    {
        public string bagageID;
        public string timeStamp;
        public string note;
        public string carID;
        public bagageInfo() { }
    }
}
