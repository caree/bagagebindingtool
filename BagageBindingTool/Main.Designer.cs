﻿namespace BagageBindingTool
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBindCarIDwithBagageID = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbCarList = new System.Windows.Forms.ComboBox();
            this.txtBagageID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRefreshCarList = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbBagageList = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnUnbindCarIDwithBagageID = new System.Windows.Forms.Button();
            this.btnSetting = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBindCarIDwithBagageID
            // 
            this.btnBindCarIDwithBagageID.Location = new System.Drawing.Point(449, 79);
            this.btnBindCarIDwithBagageID.Name = "btnBindCarIDwithBagageID";
            this.btnBindCarIDwithBagageID.Size = new System.Drawing.Size(90, 25);
            this.btnBindCarIDwithBagageID.TabIndex = 0;
            this.btnBindCarIDwithBagageID.Text = "绑定单号";
            this.btnBindCarIDwithBagageID.UseVisualStyleBackColor = true;
            this.btnBindCarIDwithBagageID.Click += new System.EventHandler(this.btnBindCarIDwithBagageID_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "车辆:";
            // 
            // cmbCarList
            // 
            this.cmbCarList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCarList.FormattingEnabled = true;
            this.cmbCarList.Location = new System.Drawing.Point(93, 67);
            this.cmbCarList.Name = "cmbCarList";
            this.cmbCarList.Size = new System.Drawing.Size(296, 20);
            this.cmbCarList.TabIndex = 2;
            // 
            // txtBagageID
            // 
            this.txtBagageID.Location = new System.Drawing.Point(93, 27);
            this.txtBagageID.Name = "txtBagageID";
            this.txtBagageID.Size = new System.Drawing.Size(296, 21);
            this.txtBagageID.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "单号:";
            // 
            // btnRefreshCarList
            // 
            this.btnRefreshCarList.Location = new System.Drawing.Point(449, 37);
            this.btnRefreshCarList.Name = "btnRefreshCarList";
            this.btnRefreshCarList.Size = new System.Drawing.Size(90, 23);
            this.btnRefreshCarList.TabIndex = 4;
            this.btnRefreshCarList.Text = "数据同步";
            this.btnRefreshCarList.UseVisualStyleBackColor = true;
            this.btnRefreshCarList.Click += new System.EventHandler(this.btnRefreshCarList_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbBagageList);
            this.groupBox1.Controls.Add(this.txtBagageID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbCarList);
            this.groupBox1.Location = new System.Drawing.Point(12, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(417, 434);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // lbBagageList
            // 
            this.lbBagageList.FormattingEnabled = true;
            this.lbBagageList.ItemHeight = 12;
            this.lbBagageList.Location = new System.Drawing.Point(93, 108);
            this.lbBagageList.Name = "lbBagageList";
            this.lbBagageList.Size = new System.Drawing.Size(296, 304);
            this.lbBagageList.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "已绑定单号:";
            // 
            // btnUnbindCarIDwithBagageID
            // 
            this.btnUnbindCarIDwithBagageID.Location = new System.Drawing.Point(449, 121);
            this.btnUnbindCarIDwithBagageID.Name = "btnUnbindCarIDwithBagageID";
            this.btnUnbindCarIDwithBagageID.Size = new System.Drawing.Size(90, 25);
            this.btnUnbindCarIDwithBagageID.TabIndex = 0;
            this.btnUnbindCarIDwithBagageID.Text = "解除绑定";
            this.btnUnbindCarIDwithBagageID.UseVisualStyleBackColor = true;
            this.btnUnbindCarIDwithBagageID.Click += new System.EventHandler(this.btnUnbindCarIDwithBagageID_Click);
            // 
            // btnSetting
            // 
            this.btnSetting.Location = new System.Drawing.Point(449, 167);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(90, 25);
            this.btnSetting.TabIndex = 0;
            this.btnSetting.Text = "参数设置";
            this.btnSetting.UseVisualStyleBackColor = true;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Silver;
            this.label4.Location = new System.Drawing.Point(31, 480);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "版本：1.0.1";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 505);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSetting);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnUnbindCarIDwithBagageID);
            this.Controls.Add(this.btnRefreshCarList);
            this.Controls.Add(this.btnBindCarIDwithBagageID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "车辆单号绑定工具";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBindCarIDwithBagageID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbCarList;
        private System.Windows.Forms.TextBox txtBagageID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRefreshCarList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnUnbindCarIDwithBagageID;
        private System.Windows.Forms.ListBox lbBagageList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSetting;
        private System.Windows.Forms.Label label4;
    }
}

