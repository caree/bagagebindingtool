﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using httpHelper;

namespace BagageBindingTool
{
    public partial class Main : Form
    {
        string host = Program.Host;
        //string host = "http://192.168.48.110:9002/";
        string token = Program.Token;
        public Main()
        {
            InitializeComponent();
            //this.cmbCarList.SelectedIndexChanged += cmbCarList_SelectedIndexChanged;
            this.Activated += Main_Activated;
        }

        void Main_Activated(object sender, EventArgs e)
        {
             host = Program.Host;
             token = Program.Token;
             this.Text = "车辆单号绑定客户端       当前用户：" + token;
        }

        void cmbCarList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbCarList.Items.Count > 0 && this.cmbCarList.SelectedIndex >= 0)
            {
                var item = this.cmbCarList.SelectedItem;
                var url = host + "bagageListBindedWithCarID";
                var content = "{\"token\": \"" + token + "\", \"carID\":\"" + item + "\"}";
                Action<object> action = (o) =>
                {
                    string log = (string)o;
                    //[{"bagageID":"demo002","timeStamp":"2014-04-29 15:40:14","note":"","carID":"xcar2"}]
                    Console.WriteLine(log);
                    Action<string> act = (_carsString) =>
                    {
                        this.lbBagageList.Items.Clear();
                        try
                        {
                            List<bagageInfo> bagageList = (List<bagageInfo>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<bagageInfo>>(_carsString);
                            string selectedCarID = this.cmbCarList.SelectedItem as string;
                            bagageList.ForEach((_bagageInfo) =>
                            {
                                this.lbBagageList.Items.Add(_bagageInfo.bagageID);
                            });
                        }
                        catch
                        {
                        }
                    };

                    this.Invoke(act, log);
                };
                triggerHttpRequest(url, content, action);
            }
        }

        private void btnRefreshCarList_Click(object sender, EventArgs e)
        {
            this.cmbCarList.Items.Clear();
            var url = host + "carListForClient";
            var content = "{\"token\": \"" + token + "\"}";
            Action<object> httpRequest_RequestCarListCompleted = (o) =>
            {
                this.cmbCarList.SelectedIndexChanged -= cmbCarList_SelectedIndexChanged;
                this.txtBagageID.Text = string.Empty;

                //[{"carID":"xcar2","carType":"02","note":"22","timeStamp":"2014-04-08 19:13:39","bagageBinded":true},{"carID":"xcar1","carType":"02","note":"11","timeStamp":"2014-04-07 14:59:16","bagageBinded":true}]
                Console.WriteLine((string)o);
                Action<string> act = (_carsString) =>
                {
                    try
                    {
                        List<carInfo> carList = (List<carInfo>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<carInfo>>(_carsString);
                        carList.ForEach((_carInfo) =>
                        {
                            Console.WriteLine(_carInfo.carID);

                            this.cmbCarList.Items.Add(_carInfo.carID);
                        });
                    }
                    catch { }

                    if (this.cmbCarList.Items.Count > 0)
                    {
                        this.cmbCarList.SelectedIndexChanged += cmbCarList_SelectedIndexChanged;
                        this.cmbCarList.SelectedIndex = 0;
                    }
                };

                this.Invoke(act, (string)o);
            };
            triggerHttpRequest(url, content, httpRequest_RequestCarListCompleted);
        }
        private void btnBindCarIDwithBagageID_Click(object sender, EventArgs e)
        {
            var bagageID = this.txtBagageID.Text;
            if (bagageID == string.Empty)
            {
                MessageBox.Show("请填写有效的单号！");
                return;
            }
            if (this.cmbCarList.Items.Count <= 0 || this.cmbCarList.SelectedIndex < 0)
            {
                MessageBox.Show("需要选择要绑定的车辆！");
                return;
            }

            var url = host + "addBagageCarBinding";
            var carID = this.cmbCarList.SelectedItem;
            var body = "{\"token\": \"" + token + "\", \"carID\":\"" + carID + "\", \"bagageID\":\"" + bagageID + "\", \"note\":\"\"}";
            Action<object> action = (o) =>
            {
                string log = (string)o;
                Console.WriteLine(log);
                if (log == "ok")
                {
                    MessageBox.Show("操作成功！");

                    Action<string> act = (_carsString) =>
                    {
                        this.txtBagageID.Text = string.Empty;
                        cmbCarList_SelectedIndexChanged(null, null);
                    };
                    this.Invoke(act, (string)o);
                }
                else
                {
                    MessageBox.Show("操作失败！");
                }

            };
            triggerHttpRequest(url, body, action);
        }


        private void btnUnbindCarIDwithBagageID_Click(object sender, EventArgs e)
        {
            if (this.cmbCarList.Items.Count <= 0 || this.cmbCarList.SelectedIndex < 0)
            {
                MessageBox.Show("需要选择要绑定的车辆！");
                return;
            }
            if (this.lbBagageList.Items.Count <= 0 || this.lbBagageList.SelectedIndex < 0)
            {
                MessageBox.Show("需要选择要解除绑定的单号！");
                return;
            }
            var carID = this.cmbCarList.SelectedItem;
            var bagageID = this.lbBagageList.SelectedItem;
            var url = host + "removeBagageCarBindingForClient";

            var body = "{\"token\": \"" + token + "\", \"carID\":\"" + carID + "\", \"bagageID\":\"" + bagageID + "\"}";
            Action<object> action = (o) =>
            {
                string log = (string)o;
                Console.WriteLine(log);
                if (log == "ok")
                {
                    MessageBox.Show("操作成功！");

                    Action<string> act = (_carsString) =>
                    {
                        cmbCarList_SelectedIndexChanged(null, null);
                    };
                    this.Invoke(act, (string)o);
                }
                else
                {
                    MessageBox.Show("操作失败！");
                }
            };
            triggerHttpRequest(url, body, action);
        }
        void triggerHttpRequest(string url, string body, Action<object> callback)
        {
            HttpWebConnect httpRequest = new HttpWebConnect();
            httpRequest.RequestCompleted += callback;
            httpRequest.TryPostData(url, body);
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            frmSetting frm = new frmSetting();
            frm.ShowDialog();
        }


    }
}
