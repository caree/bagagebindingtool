﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace BagageBindingTool
{
    public static class Program
    {

        public static string Host = "http://127.0.0.1:9002/";
        public static string Token = "";
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            initialParas();
            Application.Run(new Main());
        }

        static void initialParas()
        {
            var host = nsConfigDB.ConfigDB.getConfig("host");
            if (host != null)
            {
                Host = (string)host;
            }
            var token = nsConfigDB.ConfigDB.getConfig("token");
            if (token != null)
            {
                Token = (string)token;
            }
        }
    }
}
